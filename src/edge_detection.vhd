----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/01/2019 10:56:39 AM
-- Design Name: 
-- Module Name: edge_detection - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity edge_detection is
    Port ( clk : in STD_LOGIC;
           I : in STD_LOGIC;
           pulse : out STD_LOGIC
);
end edge_detection;

architecture Behavioral of edge_detection is

signal I_d  : std_logic := '0';  -- input delayed by 1 cycle
signal stretch_cnt : integer := 999;
signal fedge : std_logic;
begin
    
    I_d <= I when rising_edge(clk);
    process (clk)
    begin
        if (rising_edge(clk)) then
            fedge <= I_d and not I;  -- old = 1, new = 0 => falling edge
        end if;
    end process;
       

    process (clk)
    begin
     if(fedge = '1') then
        stretch_cnt <= 0; 
     elsif (stretch_cnt < 50) then 
        stretch_cnt <= stretch_cnt +1;
        pulse <= '1';
     else 
        pulse <= '0';
     end if;
    end process;     
    
end Behavioral;