library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library xil_defaultlib;
use xil_defaultlib.all;

entity AD9249_Interface_v1_0 is
	generic (
    -- Users to add parameters here

    -- User parameters ends
    -- Do not modify the parameters beyond this line


    -- Parameters of Axi Slave Bus Interface S00_AXI
    C_S00_AXI_DATA_WIDTH    : integer    := 32;
    C_S00_AXI_ADDR_WIDTH    : integer    := 6
);
	port (
    DAQ_CLK                                    : IN STD_LOGIC;
    DELAY_REF_CLK                                    : IN STD_LOGIC;
	
	
	    -- ADC Interface                 
    ADC_FC_P                                    : IN STD_LOGIC_VECTOR(1 downto 0);
    ADC_FC_N                                    : IN STD_LOGIC_VECTOR(1 downto 0);

    
    ADC_DC_P                                    : IN STD_LOGIC_VECTOR(1 downto 0);
    ADC_DC_N                                    : IN STD_LOGIC_VECTOR(1 downto 0);

    ADC_DOUTP                                 : IN STD_LOGIC_VECTOR(15 downto 0);
    ADC_DOUTN                                 : IN STD_LOGIC_VECTOR(15 downto 0);
    
    
    trigger_in          : in std_logic;
	
	
    ADC_SC_axi_clk 		: in std_logic;
    ADC0_axi_clk 		: in std_logic;
    ADC1_axi_clk 		: in std_logic;
    
    ADC0_axi_reset         : in std_logic;
    m1_axi_awready     : in std_logic;
    m1_axi_awaddr         : out std_logic_vector(31 downto 0);--32bit-->28bit: top.vhd assign high 4 bit
    m1_axi_awvalid     : out std_logic;     
    m1_axi_awlen         : out std_logic_vector(7 downto 0);
    m1_axi_awsize         : out std_logic_vector(2 downto 0);
    m1_axi_awburst     : out std_logic_vector(1 downto 0);
    m1_axi_awcache     : out std_logic_vector(3 downto 0);
    m1_axi_awprot         : out std_logic_vector(2 downto 0);
    m1_axi_wready         : in std_logic;
    m1_axi_wdata         : out std_logic_vector(63 downto 0);
    m1_axi_wvalid         : out std_logic;
    m1_axi_wstrb         : out std_logic_vector(7 downto 0);
    m1_axi_wlast         : out std_logic;
    m1_axi_bvalid         : in std_logic; 
    m1_axi_bresp         : in std_logic_vector(1 downto 0);
    m1_axi_bready         : out std_logic;
    
    ADC1_axi_reset         : in std_logic;            
    m2_axi_awready     : in std_logic;
    m2_axi_awaddr         : out std_logic_vector(31 downto 0);--32bit-->28bit: top.vhd assign high 4 bit
    m2_axi_awvalid     : out std_logic;     
    m2_axi_awlen         : out std_logic_vector(7 downto 0);
    m2_axi_awsize         : out std_logic_vector(2 downto 0);
    m2_axi_awburst     : out std_logic_vector(1 downto 0);
    m2_axi_awcache     : out std_logic_vector(3 downto 0);
    m2_axi_awprot         : out std_logic_vector(2 downto 0); 
    m2_axi_wready         : in std_logic;
    m2_axi_wdata         : out std_logic_vector(63 downto 0);
    m2_axi_wvalid         : out std_logic;
    m2_axi_wstrb         : out std_logic_vector(7 downto 0);
    m2_axi_wlast         : out std_logic;
    m2_axi_bvalid         : in std_logic; 
    m2_axi_bresp         : in std_logic_vector(1 downto 0);
    m2_axi_bready         : out std_logic;
    		-- Ports of Axi Slave Bus Interface S00_AXI
    ADC_SC_axi_aresetn    : in std_logic;
    s00_axi_awaddr    : in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
    s00_axi_awprot    : in std_logic_vector(2 downto 0);
    s00_axi_awvalid    : in std_logic;
    s00_axi_awready    : out std_logic;
    s00_axi_wdata    : in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
    s00_axi_wstrb    : in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
    s00_axi_wvalid    : in std_logic;
    s00_axi_wready    : out std_logic;
    s00_axi_bresp    : out std_logic_vector(1 downto 0);
    s00_axi_bvalid    : out std_logic;
    s00_axi_bready    : in std_logic;
    s00_axi_araddr    : in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
    s00_axi_arprot    : in std_logic_vector(2 downto 0);
    s00_axi_arvalid    : in std_logic;
    s00_axi_arready    : out std_logic;
    s00_axi_rdata    : out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
    s00_axi_rresp    : out std_logic_vector(1 downto 0);
    s00_axi_rvalid    : out std_logic;
    s00_axi_rready    : in std_logic
	);
end AD9249_Interface_v1_0;

architecture implementation of AD9249_Interface_v1_0 is
signal m1_data_gen_burst_active :std_logic;
signal m2_data_gen_burst_active :std_logic;

signal m1_fifo_wrclk      :  std_logic;
signal m1_fifo_data       :  std_logic_vector(127 downto 0);
signal m1_fifo_wren       :  std_logic;
signal m1_fifo_wrcnt      :  std_logic_vector(9 downto 0);  
signal m1_burst_addr      :  std_logic_vector(31 downto 0);  
  
signal fifo_reset      :  std_logic_vector(1 downto 0);
signal addr_rst        :  std_logic_vector(1 downto 0);       
signal m2_fifo_wrclk      :  std_logic;
signal m2_fifo_data       :  std_logic_vector(127 downto 0);
signal m2_fifo_wren       :  std_logic;
signal m2_fifo_wrcnt      :  std_logic_vector(9 downto 0);  
signal m2_burst_addr      :  std_logic_vector(31 downto 0); 

--ADC LVDS data signals
signal adc_lvds_ch_a1_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_a2_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_b1_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_b2_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_c1_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_c2_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_d1_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_d2_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_e1_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_e2_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_f1_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_f2_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_g1_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_g2_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_h1_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_h2_data     :std_logic_vector(15 downto 0);
--attribute MARK_DEBUG of adc_lvds_ch_a1_data, adc_lvds_ch_a2_data, adc_lvds_ch_b1_data, adc_lvds_ch_b2_data, adc_lvds_ch_c1_data, adc_lvds_ch_c2_data, adc_lvds_ch_d1_data, adc_lvds_ch_d2_data : signal is "TRUE";
--attribute MARK_DEBUG of adc_lvds_ch_e1_data, adc_lvds_ch_e2_data, adc_lvds_ch_f1_data, adc_lvds_ch_f2_data, adc_lvds_ch_g1_data, adc_lvds_ch_g2_data, adc_lvds_ch_h1_data, adc_lvds_ch_h2_data : signal is "TRUE"; 
signal adc_lvds_ch_a1_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_a2_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_b1_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_b2_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_c1_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_c2_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_d1_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_d2_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_e1_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_e2_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_f1_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_f2_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_g1_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_g2_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_h1_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_h2_data_dly :std_logic_vector(15 downto 0);

signal adc_fclk_bank1          :std_logic;
signal adc_fclk_bank2          :std_logic;
signal adc_clk_dis_t           :std_logic;--1 disable the adc sampling clock output
signal adc_fclk_pos            :std_logic_vector(3 downto 0);
signal adc_dclk_freq_t         :std_logic_vector(19 downto 0);
signal adc_fclk_freq_t         :std_logic_vector(19 downto 0);
signal adc_idly_cnt_out_t      :std_logic_vector(4 downto 0);
signal adc_idly_ctrl_rdy_t     :std_logic;
signal adc_idly_ld_t           :std_logic;
signal adc_idly_cnt_in_t       :std_logic_vector(4 downto 0);
signal adc_bank_sel_t          :std_logic; 


signal data_gen_trig  :std_logic_vector(1 downto 0);
signal trigger_adc0 :std_logic;
signal trigger_adc1 : std_logic;
signal trigger_pulse : std_logic;

signal data_gen_testdata_en :std_logic;
signal data_gen_adcburstenb :std_logic_vector(1 downto 0);
signal data_gen_softreset   :std_logic_vector(1 downto 0);
signal m1_data_gen_adcburstlen:std_logic_vector(31 downto 0);
signal m2_data_gen_adcburstlen:std_logic_vector(31 downto 0);


signal tied_to_ground : std_logic;

begin

tied_to_ground <= '0';

trigger_adc0 <= trigger_pulse or data_gen_trig(0);
trigger_adc1 <= trigger_pulse or data_gen_trig(1);


trigger_edge_detection : entity edge_detection
port map(
    I => trigger_in,
    clk => ADC0_axi_clk,
    pulse => trigger_pulse
);



    
-- ADC Data Trans.  
-- Address: 1000_0000 to 1FFF_FFFF     
axi_hp0_burst: entity axi_mburst
    Generic map
    (
    start_addr => X"000_0000",
    end_addr => X"7FF_FFFF"
    )
    port map(
      axi_clk       => ADC0_axi_clk,
      axi_reset     => ADC0_axi_reset,
      
      addr_rst      => addr_rst(0),
      fifo_reset    => fifo_reset(0), 
      
      fifo_wrclk    => adc_fclk_bank1,
      fifo_data     => m1_fifo_data,
      fifo_wren     => m1_fifo_wren,
      fifo_wrcnt    => m1_fifo_wrcnt, --iobus  
      burst_addr    => m1_burst_addr,
         
      axi_awready   => m1_axi_awready,
      axi_awaddr    => m1_axi_awaddr,
      axi_awvalid   => m1_axi_awvalid,
      axi_awlen     => m1_axi_awlen,
      axi_awsize    => m1_axi_awsize,
      axi_awburst   => m1_axi_awburst,
      axi_awcache   => m1_axi_awcache,
      axi_awprot    => m1_axi_awprot,
   
      axi_wready    => m1_axi_wready,
      axi_wdata     => m1_axi_wdata,
      axi_wvalid    => m1_axi_wvalid,
      axi_wstrb     => m1_axi_wstrb,
      axi_wlast     => m1_axi_wlast,
  
      axi_bvalid    => m1_axi_bvalid,
      axi_bresp     => m1_axi_bresp,
      axi_bready    => m1_axi_bready
);


axi_hp1_burst: entity axi_mburst
    Generic map
    (
    start_addr => X"000_0000",
    end_addr => X"7FF_FFFF"
    )
    port map(
      axi_clk       => ADC1_axi_clk,
      axi_reset     => ADC1_axi_reset,
      
      addr_rst      => addr_rst(0),
      fifo_reset    => fifo_reset(0), 
      
      fifo_wrclk    => adc_fclk_bank2,
      fifo_data     => m2_fifo_data,
      fifo_wren     => m2_fifo_wren,
      fifo_wrcnt    => m2_fifo_wrcnt, --iobus  
      burst_addr    => m2_burst_addr,
       
      axi_awready   => m2_axi_awready,
      axi_awaddr    => m2_axi_awaddr,
      axi_awvalid   => m2_axi_awvalid,
      axi_awlen     => m2_axi_awlen,
      axi_awsize    => m2_axi_awsize,
      axi_awburst   => m2_axi_awburst,
      axi_awcache   => m2_axi_awcache,
      axi_awprot    => m2_axi_awprot,
   
      axi_wready    => m2_axi_wready,
      axi_wdata     => m2_axi_wdata,
      axi_wvalid    => m2_axi_wvalid,
      axi_wstrb     => m2_axi_wstrb,
      axi_wlast     => m2_axi_wlast,
  
      axi_bvalid    => m2_axi_bvalid,
      axi_bresp     => m2_axi_bresp,
      axi_bready    => m2_axi_bready
);

data_gen_hp0:entity work.axi_data_gen   
  port map(
     adc_clk             => adc_fclk_bank1, 
     reset               => data_gen_softreset(0),   --iobus
                         
     trig                => trigger_adc0,    --iobus
     adcburst_enb        => data_gen_adcburstenb(0), --iobus
     adcburst_len        => m1_data_gen_adcburstlen, --iobus
     testdata_en         => data_gen_testdata_en, --iobus     
 
     adc_ch0             => adc_lvds_ch_a1_data, 
     adc_ch1             => adc_lvds_ch_b1_data, 
     adc_ch2             => adc_lvds_ch_c1_data, 
     adc_ch3             => adc_lvds_ch_d1_data, 
     adc_ch4             => adc_lvds_ch_e1_data, 
     adc_ch5             => adc_lvds_ch_f1_data, 
     adc_ch6             => adc_lvds_ch_g1_data,  
     adc_ch7             => adc_lvds_ch_h1_data,    
     
     adcdata             => m1_fifo_data, 
     adcdata_wren        => m1_fifo_wren, 
     adcburst_active     => m1_data_gen_burst_active       --ila
  );  

data_gen_hp1:entity axi_data_gen   
  port map(
     adc_clk             => adc_fclk_bank2, 
     reset               => data_gen_softreset(1),   --iobus
                         
     trig                => trigger_adc1,    --iobus
     adcburst_enb        => data_gen_adcburstenb(1), --iobus
     adcburst_len        => m2_data_gen_adcburstlen, --iobus
     testdata_en         => data_gen_testdata_en, --iobus     

     adc_ch0             => adc_lvds_ch_a2_data, 
     adc_ch1             => adc_lvds_ch_b2_data, 
     adc_ch2             => adc_lvds_ch_c2_data, 
     adc_ch3             => adc_lvds_ch_d2_data, 
     adc_ch4             => adc_lvds_ch_e2_data, 
     adc_ch5             => adc_lvds_ch_f2_data, 
     adc_ch6             => adc_lvds_ch_g2_data,  
     adc_ch7             => adc_lvds_ch_h2_data,    
     
     adcdata             => m2_fifo_data, 
     adcdata_wren        => m2_fifo_wren, 
     adcburst_active     => m2_data_gen_burst_active       --ila
  );  

ad9249_lvds_input: entity work.ad9249_lvds 
Port map ( 
    RESET   => tied_to_ground,
    SYSCLK  =>   DAQ_CLK,
    CLK200  =>   DELAY_REF_CLK,
    
    FCO1P   => ADC_FC_P(0),
    FCO1M   => ADC_FC_N(0),
    FCO2P   => ADC_FC_P(1),
    FCO2M   => ADC_FC_N(1),
        
    DCO1P   => ADC_DC_P(0),
    DCO1M   => ADC_DC_N(0),
    DCO2P   => ADC_DC_P(1),
    DCO2M   => ADC_DC_N(1),
        
    DOUTP_A1  =>  ADC_DOUTP(0),
    DOUTM_A1  =>  ADC_DOUTN(0),
    DOUTP_A2  =>  ADC_DOUTP(1),
    DOUTM_A2  =>  ADC_DOUTN(1),
    DOUTP_B1  =>  ADC_DOUTP(2),
    DOUTM_B1  =>  ADC_DOUTN(2),
    DOUTP_B2  =>  ADC_DOUTP(3),
    DOUTM_B2  =>  ADC_DOUTN(3),
    DOUTP_C1  =>  ADC_DOUTP(4),
    DOUTM_C1  =>  ADC_DOUTN(4),
    DOUTP_C2  =>  ADC_DOUTP(5),
    DOUTM_C2  =>  ADC_DOUTN(5),
    DOUTP_D1  =>  ADC_DOUTP(6),
    DOUTM_D1  =>  ADC_DOUTN(6),
    DOUTP_D2  =>  ADC_DOUTP(7),
    DOUTM_D2  =>  ADC_DOUTN(7),
    DOUTP_E1  =>  ADC_DOUTP(8),
    DOUTM_E1  =>  ADC_DOUTN(8),
    DOUTP_E2  =>  ADC_DOUTP(9),
    DOUTM_E2  =>  ADC_DOUTN(9),
    DOUTP_F1  =>  ADC_DOUTP(10),
    DOUTM_F1  =>  ADC_DOUTN(10),
    DOUTP_F2  =>  ADC_DOUTP(11),
    DOUTM_F2  =>  ADC_DOUTN(11),
    DOUTP_G1  =>  ADC_DOUTP(12),
    DOUTM_G1  =>  ADC_DOUTN(12),
    DOUTP_G2  =>  ADC_DOUTP(13),
    DOUTM_G2  =>  ADC_DOUTN(13),
    DOUTP_H1  =>  ADC_DOUTP(14),
    DOUTM_H1  =>  ADC_DOUTN(14),
    DOUTP_H2  =>  ADC_DOUTP(15),
    DOUTM_H2  =>  ADC_DOUTN(15),
    
    CH_A1_DATA_OUT => adc_lvds_ch_a1_data,
    CH_A2_DATA_OUT => adc_lvds_ch_a2_data,
    CH_B1_DATA_OUT => adc_lvds_ch_b1_data,
    CH_B2_DATA_OUT => adc_lvds_ch_b2_data,
    CH_C1_DATA_OUT => adc_lvds_ch_c1_data,
    CH_C2_DATA_OUT => adc_lvds_ch_c2_data,
    CH_D1_DATA_OUT => adc_lvds_ch_d1_data,
    CH_D2_DATA_OUT => adc_lvds_ch_d2_data,
    CH_E1_DATA_OUT => adc_lvds_ch_e1_data,
    CH_E2_DATA_OUT => adc_lvds_ch_e2_data,
    CH_F1_DATA_OUT => adc_lvds_ch_f1_data,
    CH_F2_DATA_OUT => adc_lvds_ch_f2_data,
    CH_G1_DATA_OUT => adc_lvds_ch_g1_data,
    CH_G2_DATA_OUT => adc_lvds_ch_g2_data,
    CH_H1_DATA_OUT => adc_lvds_ch_h1_data,
    CH_H2_DATA_OUT => adc_lvds_ch_h2_data,
 
    LVDS_FCLK1     => adc_fclk_bank1,
    LVDS_FCLK2     => adc_fclk_bank2,
    
    BANK_SEL       => adc_bank_sel_t,
    FCLK_POS       => adc_fclk_pos,
    DCLKFREQ       => adc_dclk_freq_t,
    FCLKFREQ       => adc_fclk_freq_t,
    
    IDLY_CNT_OUT    => adc_idly_cnt_out_t,
    IDLY_CTRL_RDY   => adc_idly_ctrl_rdy_t,
    IDLY_LD         => adc_idly_ld_t,
    IDLY_CNT_IN     => adc_idly_cnt_in_t          
); 

-- Instantiation of Axi Bus Interface S00_AXI
ADC_Config_v1_0_S00_AXI_inst : entity ADC_Config_v1_0_S00_AXI
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S00_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_ADDR_WIDTH
	)
	port map (
		S_AXI_ACLK	=> ADC_SC_axi_clk,
		S_AXI_ARESETN	=> ADC_SC_axi_aresetn,
		S_AXI_AWADDR	=> s00_axi_awaddr,
		S_AXI_AWPROT	=> s00_axi_awprot,
		S_AXI_AWVALID	=> s00_axi_awvalid,
		S_AXI_AWREADY	=> s00_axi_awready,
		S_AXI_WDATA	=> s00_axi_wdata,
		S_AXI_WSTRB	=> s00_axi_wstrb,
		S_AXI_WVALID	=> s00_axi_wvalid,
		S_AXI_WREADY	=> s00_axi_wready,
		S_AXI_BRESP	=> s00_axi_bresp,
		S_AXI_BVALID	=> s00_axi_bvalid,
		S_AXI_BREADY	=> s00_axi_bready,
		S_AXI_ARADDR	=> s00_axi_araddr,
		S_AXI_ARPROT	=> s00_axi_arprot,
		S_AXI_ARVALID	=> s00_axi_arvalid,
		S_AXI_ARREADY	=> s00_axi_arready,
		S_AXI_RDATA	=> s00_axi_rdata,
		S_AXI_RRESP	=> s00_axi_rresp,
		S_AXI_RVALID	=> s00_axi_rvalid,
		S_AXI_RREADY	=> s00_axi_rready,
		
		ADC_DCLK_FREQ         => adc_dclk_freq_t,
        ADC_FCLK_FREQ         => adc_fclk_freq_t,    
        ADC_FCLK_POS          => adc_fclk_pos,     
        ADC_BANK_SEL          => adc_bank_sel_t,
         
        ADC_IDLY_CNT_OUT      => adc_idly_cnt_out_t,
        ADC_IDLY_CTRL_RDY     => adc_idly_ctrl_rdy_t,
        ADC_IDLY_LD           => adc_idly_ld_t,
        ADC_IDLY_CNT_IN       => adc_idly_cnt_in_t, 
             
        HP_GEN_RST            => data_gen_softreset,
        HP_TRIGER             => data_gen_trig,
        HP_FIFO_RST           => fifo_reset,
        HP_ADDR_RST           => addr_rst,
        HP_BURST_EN           => data_gen_adcburstenb,
        HP_TEST_DATA_EN       => data_gen_testdata_en,
        HP0_BURST_LEN         => m1_data_gen_adcburstlen,
        HP0_BURST_ADDR        => m1_burst_addr,
        HP1_BURST_LEN         => m2_data_gen_adcburstlen,
        HP1_BURST_ADDR        => m2_burst_addr
	);




end implementation;
