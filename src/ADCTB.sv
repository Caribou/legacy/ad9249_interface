`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/04/2020 11:14:19 AM
// Design Name: 
// Module Name: ADCTB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

import axi_vip_pkg::*;
import axi_vip_0_pkg::*;
import axi_vip_1_pkg::*;

module ADCTB(

    );

              
wire [31 : 0] adc0_awaddr;  
wire [7 : 0] adc0_awlen;    
wire [2 : 0] adc0_awsize;   
wire [1 : 0] adc0_awburst;  
wire [0 : 0] adc0_awlock;   
wire [3 : 0] adc0_awcache;  
wire [2 : 0] adc0_awprot;   
wire [3 : 0] adc0_awregion; 
wire [3 : 0] adc0_awqos;    
wire adc0_awvalid;          
wire adc0_awready;         
wire [63 : 0] adc0_wdata;   
wire [7 : 0] adc0_wstrb;    
wire adc0_wlast;            
wire adc0_wvalid;           
wire adc0_wready;          
wire [1 : 0] adc0_bresp;   
wire adc0_bvalid;          
wire adc0_bready;           
wire [31 : 0] adc0_araddr;  
wire [7 : 0] adc0_arlen;    
wire [2 : 0] adc0_arsize;   
wire [1 : 0] adc0_arburst;  
wire [0 : 0] adc0_arlock;   
wire [3 : 0] adc0_arcache;  
wire [2 : 0] adc0_arprot;   
wire [3 : 0] adc0_arregion; 
wire [3 : 0] adc0_arqos;    
wire adc0_arvalid;          
wire adc0_arready;         
wire [63 : 0] adc0_rdata;  
wire [1 : 0] adc0_rresp;   
wire adc0_rlast;           
wire adc0_rvalid;          
wire adc0_rready; 

wire [31 : 0] adc1_awaddr;  
wire [7 : 0] adc1_awlen;    
wire [2 : 0] adc1_awsize;   
wire [1 : 0] adc1_awburst;  
wire [0 : 0] adc1_awlock;   
wire [3 : 0] adc1_awcache;  
wire [2 : 0] adc1_awprot;   
wire [3 : 0] adc1_awregion; 
wire [3 : 0] adc1_awqos;    
wire adc1_awvalid;          
wire adc1_awready;         
wire [63 : 0] adc1_wdata;   
wire [7 : 0] adc1_wstrb;    
wire adc1_wlast;            
wire adc1_wvalid;           
wire adc1_wready;          
wire [1 : 0] adc1_bresp;   
wire adc1_bvalid;          
wire adc1_bready;           
wire [31 : 0] adc1_araddr;  
wire [7 : 0] adc1_arlen;    
wire [2 : 0] adc1_arsize;   
wire [1 : 0] adc1_arburst;  
wire [0 : 0] adc1_arlock;   
wire [3 : 0] adc1_arcache;  
wire [2 : 0] adc1_arprot;   
wire [3 : 0] adc1_arregion; 
wire [3 : 0] adc1_arqos;    
wire adc1_arvalid;          
wire adc1_arready;         
wire [63 : 0] adc1_rdata;  
wire [1 : 0] adc1_rresp;   
wire adc1_rlast;           
wire adc1_rvalid;          
wire adc1_rready;

                
wire [5 : 0] m_axi_awaddr; 
wire [2 : 0] m_axi_awprot;   
wire m_axi_awvalid;          
wire m_axi_awready;           
wire [31 : 0] m_axi_wdata;   
wire [3 : 0] m_axi_wstrb;    
wire m_axi_wvalid;           
wire m_axi_wready;            
wire [1 : 0] m_axi_bresp;     
wire m_axi_bvalid;            
wire m_axi_bready;           
wire [5 : 0] m_axi_araddr;  
wire [2 : 0] m_axi_arprot;   
wire m_axi_arvalid;          
wire m_axi_arready;           
wire [31 : 0] m_axi_rdata;    
wire [1 : 0] m_axi_rresp;     
wire m_axi_rvalid;            
wire m_axi_rready; 

         
axi_transaction wr_reactive;

bit clk100m = 0,clk200m = 0 , f_clk=0, d_clk=0, axi_clk = 0 ,aresetn= 1; 
wire [1:0] adc_fc_p_i,adc_fc_n_i;
wire [1:0] adc_dc_p_i,adc_dc_n_i;
wire [15:0] adc_dout_p_i,adc_dout_n_i;

  
xil_axi_resp_t 	resp;

axi_vip_1_mst_t      master_agent;
axi_vip_0_slv_mem_t      slave_agent0;
axi_vip_0_slv_mem_t      slave_agent1;





bit [15 : 0] wrd0 = 16'h0000, wrd1 = 16'h0001 ,wrd2 = 16'h0002,wrd3 = 16'h0003,wrd4 = 16'h0004,wrd5 = 16'h0005,wrd6 = 16'h0006,wrd7 = 16'h0007;
bit [15 : 0] wrd0rb, wrd1rb ,wrd2rb,wrd3rb,wrd4rb,wrd5rb,wrd6rb,wrd7rb;
bit [63:0] data_block1 = 0, data_block2 = 0;  
bit [127:0] expected_data ;
bit [127:0] previous_data ;           
bit [127:0] data_beat ; 
bit [31 : 0] n_burst = 0;          
bit [31:0] word= 32'h0;

realtime sig1,sig2;

//il_axi_memory_fill_policy_t fill_policy = XIL_AXI_MEMORY_DELAY_FIXED;
  
always #10  clk100m =  ! clk100m;
always #5  clk200m =  ! clk200m;    
always #5  axi_clk =  ! axi_clk; 

//ADC clocks

always #10  d_clk =  ! d_clk;
always #70  f_clk =  ! f_clk;



//SimBD_wrapper ADCBD(
//.ADC_DC_N_0({d_clk_n,d_clk_n}),
//.ADC_DC_P_0({d_clk_p,d_clk_p}),
////.ADC_DOUTN_0(), 
////.ADC_DOUTP_0(), 
//.ADC_FC_N_0({f_clk_n,f_clk_n}),
//.ADC_FC_P_0({f_clk_p,f_clk_p}),
//.aclk(axi_clk),
//.aresetn(aresetn),
//.clk100m_0(clk100m),
//.clk200m_0(clk200m) 
//);


axi_vip_0 AXISlave0 (
      .aclk(axi_clk),                      // input wire aclk
      .aresetn(aresetn),                // input wire aresetn
      .s_axi_awaddr(adc0_awaddr),      // input wire [31 : 0] s_axi_awaddr
      .s_axi_awlen(adc0_awlen),        // input wire [7 : 0] s_axi_awlen
      .s_axi_awsize(adc0_awsize),      // input wire [2 : 0] s_axi_awsize
      .s_axi_awburst(adc0_awburst),    // input wire [1 : 0] s_axi_awburst
//      .s_axi_awlock(adc0_awlock),      // input wire [0 : 0] s_axi_awlock
      .s_axi_awcache(adc0_awcache),    // input wire [3 : 0] s_axi_awcache
      .s_axi_awprot(adc0_awprot),      // input wire [2 : 0] s_axi_awprot
      .s_axi_awregion(adc0_awregion),  // input wire [3 : 0] s_axi_awregion
      .s_axi_awqos(adc0_awqos),        // input wire [3 : 0] s_axi_awqos
      .s_axi_awvalid(adc0_awvalid),    // input wire s_axi_awvalid
      .s_axi_awready(adc0_awready),    // output wire s_axi_awready
      .s_axi_wdata(adc0_wdata),        // input wire [63 : 0] s_axi_wdata
      .s_axi_wstrb(adc0_wstrb),        // input wire [7 : 0] s_axi_wstrb
      .s_axi_wlast(adc0_wlast),        // input wire s_axi_wlast
      .s_axi_wvalid(adc0_wvalid),      // input wire s_axi_wvalid
      .s_axi_wready(adc0_wready),      // output wire s_axi_wready
      .s_axi_bresp(adc0_bresp),        // output wire [1 : 0] s_axi_bresp
      .s_axi_bvalid(adc0_bvalid),      // output wire s_axi_bvalid
      .s_axi_bready(adc0_bready)      // input wire s_axi_bready
//      .s_axi_araddr(adc0_araddr),      // input wire [31 : 0] s_axi_araddr
//      .s_axi_arlen(adc0_arlen),        // input wire [7 : 0] s_axi_arlen
//      .s_axi_arsize(adc0_arsize),      // input wire [2 : 0] s_axi_arsize
//      .s_axi_arburst(adc0_arburst),    // input wire [1 : 0] s_axi_arburst
//      .s_axi_arlock(adc0_arlock),      // input wire [0 : 0] s_axi_arlock
//      .s_axi_arcache(adc0_arcache),    // input wire [3 : 0] s_axi_arcache
//      .s_axi_arprot(adc0_arprot),      // input wire [2 : 0] s_axi_arprot
//      .s_axi_arregion(adc0_arregion),  // input wire [3 : 0] s_axi_arregion
//      .s_axi_arqos(adc0_arqos),        // input wire [3 : 0] s_axi_arqos
//      .s_axi_arvalid(adc0_arvalid),    // input wire s_axi_arvalid
//      .s_axi_arready(adc0_arready),    // output wire s_axi_arready
//      .s_axi_rdata(adc0_rdata),        // output wire [63 : 0] s_axi_rdata
//      .s_axi_rresp(adc0_rresp),        // output wire [1 : 0] s_axi_rresp
//      .s_axi_rlast(adc0_rlast),        // output wire s_axi_rlast
//      .s_axi_rvalid(adc0_rvalid),      // output wire s_axi_rvalid
//      .s_axi_rready(adc0_rready)      // input wire s_axi_rready
    );    
   
axi_vip_0 AXISlave1 (
          .aclk(axi_clk),                      // input wire aclk
          .aresetn(aresetn),                // input wire aresetn
          .s_axi_awaddr(adc1_awaddr),      // input wire [31 : 0] s_axi_awaddr
          .s_axi_awlen(adc1_awlen),        // input wire [7 : 0] s_axi_awlen
          .s_axi_awsize(adc1_awsize),      // input wire [2 : 0] s_axi_awsize
          .s_axi_awburst(adc1_awburst),    // input wire [1 : 0] s_axi_awburst
//          .s_axi_awlock(adc1_awlock),      // input wire [0 : 0] s_axi_awlock
          .s_axi_awcache(adc1_awcache),    // input wire [3 : 0] s_axi_awcache
          .s_axi_awprot(adc1_awprot),      // input wire [2 : 0] s_axi_awprot
          .s_axi_awregion(adc1_awregion),  // input wire [3 : 0] s_axi_awregion
          .s_axi_awqos(adc1_awqos),        // input wire [3 : 0] s_axi_awqos
          .s_axi_awvalid(adc1_awvalid),    // input wire s_axi_awvalid
          .s_axi_awready(adc1_awready),    // output wire s_axi_awready
          .s_axi_wdata(adc1_wdata),        // input wire [63 : 0] s_axi_wdata
          .s_axi_wstrb(adc1_wstrb),        // input wire [7 : 0] s_axi_wstrb
          .s_axi_wlast(adc1_wlast),        // input wire s_axi_wlast
          .s_axi_wvalid(adc1_wvalid),      // input wire s_axi_wvalid
          .s_axi_wready(adc1_wready),      // output wire s_axi_wready
          .s_axi_bresp(adc1_bresp),        // output wire [1 : 0] s_axi_bresp
          .s_axi_bvalid(adc1_bvalid),      // output wire s_axi_bvalid
          .s_axi_bready(adc1_bready)      // input wire s_axi_bready
//          .s_axi_araddr(adc1_araddr),      // input wire [31 : 0] s_axi_araddr
//          .s_axi_arlen(adc1_arlen),        // input wire [7 : 0] s_axi_arlen
//          .s_axi_arsize(adc1_arsize),      // input wire [2 : 0] s_axi_arsize
//          .s_axi_arburst(adc1_arburst),    // input wire [1 : 0] s_axi_arburst
//          .s_axi_arlock(adc1_arlock),      // input wire [0 : 0] s_axi_arlock
//          .s_axi_arcache(adc1_arcache),    // input wire [3 : 0] s_axi_arcache
//          .s_axi_arprot(adc1_arprot),      // input wire [2 : 0] s_axi_arprot
//          .s_axi_arregion(adc1_arregion),  // input wire [3 : 0] s_axi_arregion
//          .s_axi_arqos(adc1_arqos),        // input wire [3 : 0] s_axi_arqos
//          .s_axi_arvalid(adc1_arvalid),    // input wire s_axi_arvalid
//          .s_axi_arready(adc1_arready),    // output wire s_axi_arready
//          .s_axi_rdata(adc1_rdata),        // output wire [63 : 0] s_axi_rdata
//          .s_axi_rresp(adc1_rresp),        // output wire [1 : 0] s_axi_rresp
//          .s_axi_rlast(adc1_rlast),        // output wire s_axi_rlast
//          .s_axi_rvalid(adc1_rvalid),      // output wire s_axi_rvalid
//          .s_axi_rready(adc1_rready)      // input wire s_axi_rready
        );

axi_vip_1 AXIlitemaster (
          .aclk(axi_clk),                    // input wire aclk
          .aresetn(aresetn),              // input wire aresetn
          .m_axi_awaddr(m_axi_awaddr),    // output wire [31 : 0] m_axi_awaddr
          .m_axi_awprot(m_axi_awprot),    // output wire [2 : 0] m_axi_awprot
          .m_axi_awvalid(m_axi_awvalid),  // output wire m_axi_awvalid
          .m_axi_awready(m_axi_awready),  // input wire m_axi_awready
          .m_axi_wdata(m_axi_wdata),      // output wire [31 : 0] m_axi_wdata
          .m_axi_wstrb(m_axi_wstrb),      // output wire [3 : 0] m_axi_wstrb
          .m_axi_wvalid(m_axi_wvalid),    // output wire m_axi_wvalid
          .m_axi_wready(m_axi_wready),    // input wire m_axi_wready
          .m_axi_bresp(m_axi_bresp),      // input wire [1 : 0] m_axi_bresp
          .m_axi_bvalid(m_axi_bvalid),    // input wire m_axi_bvalid
          .m_axi_bready(m_axi_bready),    // output wire m_axi_bready
          .m_axi_araddr(m_axi_araddr),    // output wire [31 : 0] m_axi_araddr
          .m_axi_arprot(m_axi_arprot),    // output wire [2 : 0] m_axi_arprot
          .m_axi_arvalid(m_axi_arvalid),  // output wire m_axi_arvalid
          .m_axi_arready(m_axi_arready),  // input wire m_axi_arready
          .m_axi_rdata(m_axi_rdata),      // input wire [31 : 0] m_axi_rdata
          .m_axi_rresp(m_axi_rresp),      // input wire [1 : 0] m_axi_rresp
          .m_axi_rvalid(m_axi_rvalid),    // input wire m_axi_rvalid
          .m_axi_rready(m_axi_rready)    // output wire m_axi_rready
        );       
 
 
 AD9249_model ADCsim(
 
        .dclk_in(d_clk),
        .fclk_in(f_clk),
        .ADC_FC_P(adc_fc_p_i),
        .ADC_FC_N(adc_fc_n_i),
        .ADC_DC_P(adc_dc_p_i),
        .ADC_DC_N(adc_dc_n_i),
        .ADC_DOUT_P(adc_dout_p_i),
        .ADC_DOUT_N(adc_dout_n_i)
 );

            
AD9249_Interface_v1_0  ADCInterface(

        .DAQ_CLK(clk100m),     
        .DELAY_REF_CLK(clk200m),      
                                  
     
        .ADC_DC_N(adc_dc_n_i),
        .ADC_DC_P(adc_dc_p_i),            
        .ADC_FC_N(adc_fc_n_i),
        .ADC_FC_P(adc_fc_p_i),            
    
                     
        .ADC_DOUTP(adc_dout_p_i),    
        .ADC_DOUTN(adc_dout_n_i),    
                                         
        .ADC_SC_axi_clK(axi_clk),
        .ADC0_axi_clk(axi_clk),
        .ADC1_axi_clk(axi_clk), 
                     
        .ADC0_axi_reset(aresetn), 
        .m1_axi_awready(adc0_awready),
        .m1_axi_awaddr(adc0_awaddr),
        .m1_axi_awvalid(adc0_awvalid),
        .m1_axi_awlen(adc0_awlen), 
        .m1_axi_awsize(adc0_awsize),
        .m1_axi_awbursT(adc0_awburst),
        .m1_axi_awcache(adc0_awcache),
        .m1_axi_awprot(adc0_awprot),
        .m1_axi_wready(adc0_wready),
        .m1_axi_wdata (adc0_wdata),
        .m1_axi_wvalid(adc0_wvalid),
        .m1_axi_wstrb(adc0_wstrb),
        .m1_axi_wlast(adc0_wlast), 
        .m1_axi_bvalid(adc0_bvalid),
        .m1_axi_bresp(adc0_bresp), 
        .m1_axi_bready(adc0_bready),
                     
        .ADC1_axi_reset(aresetn), 
        .m2_axi_awready(adc1_awready),
        .m2_axi_awaddr(adc1_awaddr),
        .m2_axi_awvalid(adc1_awvalid),
        .m2_axi_awlen(adc1_awlen), 
        .m2_axi_awsize(adc1_awsize),
        .m2_axi_awbursT(adc1_awburst),
        .m2_axi_awcache(adc1_awcache),
        .m2_axi_awprot(adc1_awprot),
        .m2_axi_wready(adc1_wready),
        .m2_axi_wdata (adc1_wdata),
        .m2_axi_wvalid(adc1_wvalid),
        .m2_axi_wstrb(adc1_wstrb),
        .m2_axi_wlast(adc1_wlast), 
        .m2_axi_bvalid(adc1_bvalid),
        .m2_axi_bresp(adc1_bresp), 
        .m2_axi_bready(adc1_bready),
        
        .ADC_SC_axi_aresetn(aresetn),
        .s00_axi_awaddr(m_axi_awaddr),
        .s00_axi_awprot(m_axi_awprot),
        .s00_axi_awvalid(m_axi_awvalid),
        .s00_axi_awready(m_axi_awready),
        .s00_axi_wdata(m_axi_wdata),
        .s00_axi_wstrb(m_axi_wstrb),
        .s00_axi_wvalid(m_axi_wvalid),
        .s00_axi_wready(m_axi_wready),
        .s00_axi_bresp(m_axi_bresp),
        .s00_axi_bvalid(m_axi_bvalid),
        .s00_axi_bready(m_axi_bready),
        .s00_axi_araddr(m_axi_araddr),
        .s00_axi_arprot(m_axi_arprot),
        .s00_axi_arvalid(m_axi_arvalid),
        .s00_axi_arready(m_axi_arready),
        .s00_axi_rdata(m_axi_rdata),
        .s00_axi_rresp(m_axi_rresp),
        .s00_axi_rvalid(m_axi_rvalid),
        .s00_axi_rready(m_axi_rready)
            
);





initial begin
    //Assert the reset
    aresetn = 0;
    #340ns
    // Release the reset
    aresetn = 1;
end



initial begin
// Step 4 - Create a new agent
master_agent = new("ADC slow controls", AXIlitemaster.inst.IF);
slave_agent0 = new("ADC Data 0",AXISlave0.inst.IF);
slave_agent1 = new("ADC Data 1",AXISlave1.inst.IF);

// Step 5 - Start the agent
master_agent.start_master();
slave_agent0.start_slave();
slave_agent1.start_slave();

gen_wready();

slave_agent0.set_verbosity(40);
slave_agent1.set_verbosity(40);

slave_agent0.mem_model.set_memory_fill_policy(XIL_AXI_MEMORY_FILL_FIXED);
slave_agent1.mem_model.set_memory_fill_policy(XIL_AXI_MEMORY_FILL_FIXED);

slave_agent0.mem_model.set_default_memory_value(64'hFFFFFFFFFFFFBBBB);
slave_agent1.mem_model.set_default_memory_value(64'hFFFFFFFFFFFFBBBB);


reset_all();

#250 

set_delay(0,0);
set_delay(0,1);

//#100   
//@(posedge adc_dc_p_i) // wait for sig to goto 0
//sig1 = $realtime ;
//@(posedge axi_clk)      // wait for enable to change its value
//sig2= $realtime - sig1;
//$display("delay %d",sig2);

#250


for(int n = 32'h1; n <= 32'hFF; n++) begin
    //reset_all();
    reset_data_gen();
    reset_address();
    
    #1000 
    n_burst = n;
    
    set_burst_length(n_burst);
    enable_testdata_ro();
    
    #1000
    
    send_trigger();
    
    for(int i=0; i<n_burst;i++) begin
        slave_agent0.monitor.item_collected_port.get(wr_reactive);
    end
    
    //slave_agent0.wr_driver.get_wr_reactive(wr_reactive);
    //fill_reactive(wr_reactive);
    //slave_agent0.wr_driver.send(wr_reactive);
    
    #1000
    
    for(int i = 0; i < n_burst*2048;i+=16) begin
    
        data_block1=slave_agent0.mem_model.backdoor_memory_read(i);
        data_block2=slave_agent0.mem_model.backdoor_memory_read(i+8);              
        
        
        #10
            
        if(data_block1 != expected_data[127:64] && data_block2 != expected_data[63:0]) begin
            $strobe ("FAIL  address : %0h data block %8h %8h vs %16h",i,data_block1,data_block2,expected_data);
    //        $strobe("%4h vs %4h",wrd0,wrd0rb);
    //        $strobe("%4h vs %4h",wrd1,wrd1rb);
    //        $strobe("%4h vs %4h",wrd2,wrd2rb);
    //        $strobe("%4h vs %4h",wrd3,wrd3rb);
    //        $strobe("%4h vs %4h",wrd4,wrd4rb);
    //        $strobe("%4h vs %4h",wrd5,wrd5rb);
    //        $strobe("%4h vs %4h",wrd6,wrd6rb);
    //        $strobe("%4h vs %4h",wrd7,wrd7rb);
            end
        else begin
            //$strobe ("MATCH address : %0h data block %8h %8h vs %16h",i,data_block1,data_block2,expected_data);
    //        $strobe("%4h vs %4h",wrd0,wrd0rb);
    //        $strobe("%4h vs %4h",wrd1,wrd1rb);
    //        $strobe("%4h vs %4h",wrd2,wrd2rb);
    //        $strobe("%4h vs %4h",wrd3,wrd3rb);
    //        $strobe("%4h vs %4h",wrd4,wrd4rb);
    //        $strobe("%4h vs %4h",wrd5,wrd5rb);
    //        $strobe("%4h vs %4h",wrd6,wrd6rb);
    //        $strobe("%4h vs %4h",wrd7,wrd7rb);
            end
            
       #10 
       
       increment_wrd();
    
    
    end 
    reset_wrd();

end

//#100 

//enable_ADC_ro();

//#100


//for(int n = 1 ; n <= 32'hF; n++) begin
    
//    #2000
    
//    reset_all();

//    #1000 
    
//    n_burst = n;
    
//    set_burst_length(n_burst);
//    //enable_testdata_ro();
   
//    #1000
    
//    send_trigger();
    
//    for(int i=0; i<n_burst;i++) begin
//        slave_agent0.monitor.item_collected_port.get(wr_reactive);
//    end
    
//    #100
    
//    for(int i = 0; i < n_burst*2048;i+=16) begin
    
//        data_block1=slave_agent0.mem_model.backdoor_memory_read(i);
//        data_block2=slave_agent0.mem_model.backdoor_memory_read(i+8);              
        

//        $display("READ DATA : %16h",{data_block1,data_block2});
//;
   
//    end
                            
   
//end 

$display("finish");
  
end

assign expected_data[127:0] = {wrd3[15:0],wrd2[15:0],wrd1[15:0],wrd0[15:0],wrd7[15:0],wrd6[15:0],wrd5[15:0],wrd4[15:0]};  
assign wrd0rb = data_block1[15:0];
assign wrd1rb = data_block1[31:16];
assign wrd2rb = data_block1[47:32];
assign wrd3rb = data_block1[63:48];
assign wrd4rb = data_block2[15:0];
assign wrd5rb = data_block2[31:16];
assign wrd6rb = data_block2[47:32];
assign wrd7rb = data_block2[63:48];


task increment_wrd();
    wrd0 = wrd0+16'h0008;
    wrd1 = wrd1+16'h0008;
    wrd2 = wrd2+16'h0008;
    wrd3 = wrd3+16'h0008;
    wrd4 = wrd4+16'h0008;
    wrd5 = wrd5+16'h0008;
    wrd6 = wrd6+16'h0008;
    wrd7 = wrd7+16'h0008;
endtask :increment_wrd

task reset_wrd();
    wrd0 = 16'h0000;
    wrd1 = 16'h0001;
    wrd2 = 16'h0002;
    wrd3 = 16'h0003;
    wrd4 = 16'h0004;
    wrd5 = 16'h0005;
    wrd6 = 16'h0006;
    wrd7 = 16'h0007;
endtask :reset_wrd

// task to set slave AXI VIP wready policy to always asserted
task gen_wready();
    axi_ready_gen       wready_gen, wready_gen2;
    wready_gen = slave_agent0.wr_driver.create_ready("wready");
    wready_gen.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
    slave_agent0.wr_driver.send_wready(wready_gen);
    wready_gen2 = slave_agent1.wr_driver.create_ready("wready");
    wready_gen2.set_ready_policy(XIL_AXI_READY_GEN_NO_BACKPRESSURE);
    slave_agent1.wr_driver.send_wready(wready_gen2);
endtask :gen_wready
 
task send_trigger();
    //Send trigger 
    master_agent.AXI4LITE_WRITE_BURST(32'h44A0_0000 + 32'h4,0,32'hF,resp);
    #100
    master_agent.AXI4LITE_WRITE_BURST(32'h44A0_0000 + 32'h4,0,32'h0,resp);
endtask :send_trigger 


task reset_data_gen();
    //Reset data gem 
    master_agent.AXI4LITE_WRITE_BURST(32'h44A0_0000 + 32'h0,0,32'hF,resp);
    #100
    master_agent.AXI4LITE_WRITE_BURST(32'h44A0_0000 + 32'h0,0,32'h0,resp);
    
endtask :reset_data_gen

task reset_fifo();
   
    //Reset fifos
    master_agent.AXI4LITE_WRITE_BURST(32'h44A0_0000 + 32'h8,0,32'hF,resp);
    #2500
    master_agent.AXI4LITE_WRITE_BURST(32'h44A0_0000 + 32'h8,0,32'h0,resp);
//    #600 

endtask :reset_fifo 

task reset_address();
    //reset burst start address
    master_agent.AXI4LITE_WRITE_BURST(32'h44A0_0000 + 32'hC,0,32'hF,resp);
    #100
    master_agent.AXI4LITE_WRITE_BURST(32'h44A0_0000 + 32'hC,0,32'h0,resp);

endtask :reset_address 

 
task reset_all();

    reset_data_gen();
    reset_fifo();
    reset_address();

endtask :reset_all 
 
task enable_testdata_ro();
    //enable burst 
    master_agent.AXI4LITE_WRITE_BURST(32'h44A0_0000 + 32'h10,0,32'hF,resp);
    //Enable test data 
    master_agent.AXI4LITE_WRITE_BURST(32'h44A0_0000 + 32'h14,0,32'hF,resp);
     
endtask :enable_testdata_ro 


task enable_ADC_ro();
    //enable burst 
    master_agent.AXI4LITE_WRITE_BURST(32'h44A0_0000 + 32'h10,0,32'hF,resp);
    //disable test data 
    master_agent.AXI4LITE_WRITE_BURST(32'h44A0_0000 + 32'h14,0,32'h0,resp);
endtask :enable_ADC_ro 


 
task set_burst_length;
    input [31:0] brst_length; 
begin
 //Set ADC burst length
    $display ("burst size: %0h",brst_length);
    master_agent.AXI4LITE_WRITE_BURST(32'h44A0_0000 + 32'h18,0,brst_length,resp);
    master_agent.AXI4LITE_WRITE_BURST(32'h44A0_0000 + 32'h1C,0,brst_length,resp);
end
endtask :set_burst_length

task set_delay;
    input [4:0] tapvalue;
    input banksel;
begin
    //bit [31:0] word= 32'h0;
    word[7:0] = {tapvalue,banksel};
    //$strobe("word : %8b",word);
    master_agent.AXI4LITE_WRITE_BURST(32'h44A0_0000 + 32'h34,0,word,resp);
    word[7:0] = {1'b1,tapvalue,banksel};
    //$strobe("word : %8b",word);
    master_agent.AXI4LITE_WRITE_BURST(32'h44A0_0000 + 32'h34,0,word,resp);
    word[7:0] = {1'b0,tapvalue,banksel};
    //$strobe("word : %8b",word);
    master_agent.AXI4LITE_WRITE_BURST(32'h44A0_0000 + 32'h34,0,word,resp);

end
endtask :set_delay




  //fill reactive response for write transaction
 function automatic void fill_reactive(inout axi_transaction t);
    xil_axi_resp_t       bresp;
    xil_axi_user_beat  buser;
    buser = $urandom_range(0,1<< 0 -1);
    bresp = XIL_AXI_RESP_OKAY; 
    t.set_buser(buser);
    t.set_bresp(bresp);
  endfunction: fill_reactive

    
endmodule
