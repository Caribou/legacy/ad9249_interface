----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/10/2014 09:06:17 AM
-- Design Name: 
-- Module Name: ads52j90_lvds - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
Library UNISIM;
use UNISIM.vcomponents.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ad9249_lvds is
    Port ( 
		   RESET   : in STD_LOGIC;
		   CLK200  : in STD_LOGIC;
		   SYSCLK  : in STD_LOGIC;
		   
           FCO1P     : IN STD_LOGIC;
           FCO1M     : IN STD_LOGIC;
           FCO2P     : IN STD_LOGIC;
           FCO2M     : IN STD_LOGIC;
               
           DCO1P     : IN STD_LOGIC;
           DCO1M     : IN STD_LOGIC;
           DCO2P     : IN STD_LOGIC;
           DCO2M     : IN STD_LOGIC;
               
           DOUTP_A1  : IN STD_LOGIC;
           DOUTM_A1  : IN STD_LOGIC;
           DOUTP_A2  : IN STD_LOGIC;
           DOUTM_A2  : IN STD_LOGIC;
           DOUTP_B1  : IN STD_LOGIC;
           DOUTM_B1  : IN STD_LOGIC;
           DOUTP_B2  : IN STD_LOGIC;
           DOUTM_B2  : IN STD_LOGIC;
           DOUTP_C1  : IN STD_LOGIC;
           DOUTM_C1  : IN STD_LOGIC;
           DOUTP_C2  : IN STD_LOGIC;
           DOUTM_C2  : IN STD_LOGIC;
           DOUTP_D1  : IN STD_LOGIC;
           DOUTM_D1  : IN STD_LOGIC;
           DOUTP_D2  : IN STD_LOGIC;
           DOUTM_D2  : IN STD_LOGIC;
           DOUTP_E1  : IN STD_LOGIC;
           DOUTM_E1  : IN STD_LOGIC;
           DOUTP_E2  : IN STD_LOGIC;
           DOUTM_E2  : IN STD_LOGIC;
           DOUTP_F1  : IN STD_LOGIC;
           DOUTM_F1  : IN STD_LOGIC;
           DOUTP_F2  : IN STD_LOGIC;
           DOUTM_F2  : IN STD_LOGIC;
           DOUTP_G1  : IN STD_LOGIC;
           DOUTM_G1  : IN STD_LOGIC;
           DOUTP_G2  : IN STD_LOGIC;
           DOUTM_G2  : IN STD_LOGIC;
           DOUTP_H1  : IN STD_LOGIC;
           DOUTM_H1  : IN STD_LOGIC;
           DOUTP_H2  : IN STD_LOGIC;
           DOUTM_H2  : IN STD_LOGIC;
           
           CH_A1_DATA_OUT  : out STD_LOGIC_VECTOR(15 downto 0);
           CH_B1_DATA_OUT  : out STD_LOGIC_VECTOR(15 downto 0);
           CH_C1_DATA_OUT  : out STD_LOGIC_VECTOR(15 downto 0);
           CH_D1_DATA_OUT  : out STD_LOGIC_VECTOR(15 downto 0);
           CH_E1_DATA_OUT  : out STD_LOGIC_VECTOR(15 downto 0);
           CH_F1_DATA_OUT  : out STD_LOGIC_VECTOR(15 downto 0);
           CH_G1_DATA_OUT  : out STD_LOGIC_VECTOR(15 downto 0);
           CH_H1_DATA_OUT  : out STD_LOGIC_VECTOR(15 downto 0);
           CH_A2_DATA_OUT  : out STD_LOGIC_VECTOR(15 downto 0);
           CH_B2_DATA_OUT  : out STD_LOGIC_VECTOR(15 downto 0);
           CH_C2_DATA_OUT  : out STD_LOGIC_VECTOR(15 downto 0);
           CH_D2_DATA_OUT  : out STD_LOGIC_VECTOR(15 downto 0);
           CH_E2_DATA_OUT  : out STD_LOGIC_VECTOR(15 downto 0);
           CH_F2_DATA_OUT  : out STD_LOGIC_VECTOR(15 downto 0);
           CH_G2_DATA_OUT  : out STD_LOGIC_VECTOR(15 downto 0);
           CH_H2_DATA_OUT  : out STD_LOGIC_VECTOR(15 downto 0);
           
           LVDS_FCLK1      : out STD_LOGIC;
           LVDS_FCLK2      : out STD_LOGIC;
            
           BANK_SEL        : in STD_LOGIC;
           FCLK_POS        : in STD_LOGIC_VECTOR(3 downto 0);
           DCLKFREQ        : out STD_LOGIC_VECTOR(19 downto 0);
           FCLKFREQ        : out STD_LOGIC_VECTOR(19 downto 0);
            
           IDLY_CNT_OUT    : out std_logic_vector(4 downto 0);
           IDLY_CTRL_RDY   : out std_logic;
           IDLY_LD         : in  std_logic;
           IDLY_CNT_IN     : in  std_logic_vector(4 downto 0)          
		   );
end ad9249_lvds;
  


architecture Behavioral of ad9249_lvds is
--------------------------------------Signal declaration---------------------------
signal tied_to_ground : std_logic;
--clock
signal fclk1 : std_logic;--Frame clock of ADC data;
signal dclk1 : std_logic;--Bit clock of ADC data;
signal fclk2 : std_logic;--Frame clock of ADC data;
signal fclk2a : std_logic;
signal dclk2 : std_logic;--Bit clock of ADC data;

signal dclk1_freq :std_logic_vector(19 downto 0);
signal fclk1_freq :std_logic_vector(19 downto 0);
signal dclk2_freq :std_logic_vector(19 downto 0);
signal fclk2_freq :std_logic_vector(19 downto 0);

signal fclk_pos_bank1 :std_logic_vector(3 downto 0);
signal fclk_pos_bank2 :std_logic_vector(3 downto 0);

signal adc_ch_a1_data :std_logic_vector(13 downto 0);
signal adc_ch_b1_data :std_logic_vector(13 downto 0);
signal adc_ch_c1_data :std_logic_vector(13 downto 0);
signal adc_ch_d1_data :std_logic_vector(13 downto 0);
signal adc_ch_e1_data :std_logic_vector(13 downto 0);
signal adc_ch_f1_data :std_logic_vector(13 downto 0);
signal adc_ch_g1_data :std_logic_vector(13 downto 0);
signal adc_ch_h1_data :std_logic_vector(13 downto 0);
signal adc_ch_a2_data :std_logic_vector(13 downto 0);
signal adc_ch_b2_data :std_logic_vector(13 downto 0);
signal adc_ch_c2_data :std_logic_vector(13 downto 0);
signal adc_ch_d2_data :std_logic_vector(13 downto 0);
signal adc_ch_e2_data :std_logic_vector(13 downto 0);
signal adc_ch_f2_data :std_logic_vector(13 downto 0);
signal adc_ch_g2_data :std_logic_vector(13 downto 0);
signal adc_ch_h2_data :std_logic_vector(13 downto 0);


attribute MARK_DEBUG : string;
attribute MARK_DEBUG of adc_ch_a1_data,adc_ch_a2_data : signal is "TRUE";

signal dclk1_idly_cnt_out    : std_logic_vector(4 downto 0);
signal dclk1_idly_ctrl_rdy   : std_logic;
signal dclk1_idly_ld         : std_logic;
signal dclk1_idly_cnt_in     : std_logic_vector(4 downto 0); 
 
signal dclk2_idly_cnt_out    : std_logic_vector(4 downto 0);
signal dclk2_idly_ctrl_rdy   : std_logic;
signal dclk2_idly_ld         : std_logic;
signal dclk2_idly_cnt_in     : std_logic_vector(4 downto 0);  

component dclk_delay
port(
      RESET      : in std_logic;
	  DCLKP_IN   : in std_logic;
	  DCLKN_IN   : in std_logic;
	  REFCLK_IN  : in std_logic;
	  
	  --Control ports
      CNT_OUT    : out std_logic_vector(4 downto 0);
      CTRL_RDY   : out std_logic;
      IDLY_LD    : in  std_logic;
      CNT_IN     : in  std_logic_vector(4 downto 0);
	  
	  DCLK_OUT   :out std_logic
	  );
end component;
component dclk2_delay
port(
      RESET      : in std_logic;
	  DCLKP_IN   : in std_logic;
	  DCLKN_IN   : in std_logic;
	  REFCLK_IN  : in std_logic;
	  
	  --Control ports
	  CNT_OUT    : out std_logic_vector(4 downto 0);
	  IDLY_LD    : in  std_logic;
	  CNT_IN     : in  std_logic_vector(4 downto 0);
	 	  
	  DCLK_OUT   :out std_logic
	  );
end component;
	  
component ads52j90_lvds_ch
    Port (
    RESET :in std_logic;
	FCLK  :in std_logic;
    DCLK  :in std_logic;
    DOUTP :in std_logic;
    DOUTM :in std_logic;
    DATA_OUT :out std_logic_vector(13 downto 0);
	latch_p_s : std_logic_vector(3 downto 0)
     );
end component;


--frequency counter 
component frequency_counter 
   Generic(
           SYS_CLOCK_PERIOD : integer range 4 to 250 := 10 
          );
     Port ( CLOCK_PROBE : in STD_LOGIC;
            SYSCLK : in STD_LOGIC;
            RESET : in STD_LOGIC;
            FREQUENCY : out STD_LOGIC_VECTOR ( 19 downto 0)
            );
end component;

begin
tied_to_ground <= '0';

process(SYSCLK,BANK_SEL)
begin
    if rising_edge(SYSCLK) then
        if BANK_SEL = '0' then
            fclk_pos_bank1 <= FCLK_POS;
            dclk1_idly_ld  <= IDLY_LD;            
            dclk1_idly_cnt_in <= IDLY_CNT_IN;
            DCLKFREQ <= dclk1_freq;
            FCLKFREQ <= fclk1_freq;
           
            IDLY_CNT_OUT  <= dclk1_idly_cnt_out;
            IDLY_CTRL_RDY <= dclk1_idly_ctrl_rdy;
            
         elsif BANK_SEL = '1' then 
            fclk_pos_bank2 <= FCLK_POS;
            dclk2_idly_ld  <= IDLY_LD;
            dclk2_idly_cnt_in <= IDLY_CNT_IN;
            DCLKFREQ <= dclk2_freq;
            FCLKFREQ <= fclk2_freq;         
            IDLY_CNT_OUT  <= dclk2_idly_cnt_out;
            IDLY_CTRL_RDY <= dclk2_idly_ctrl_rdy;
         else
            NULL;
         end if;
    end if;
end process;

FCLK1_IBUFGDS: IBUFGDS
 generic map (
   DIFF_TERM => TRUE, 
   IBUF_LOW_PWR => FALSE, 
   IOSTANDARD => "DEFAULT")
 port map (
   O => fclk1, 
   I  => FCO1P, 
   IB => FCO1M 
 );
 
FCLK2_IBUFDS:IBUFDS
 generic map (
 DIFF_TERM => TRUE, -- Differential Termination
 IBUF_LOW_PWR => FALSE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
 IOSTANDARD => "DEFAULT")
 port map (
 O => fclk2a, -- Buffer output
 I => FCO2P, -- Diff_p buffer input (connect directly to top-level port)
 IB => FCO2M -- Diff_n buffer input (connect directly to top-level port)
 );
 
-- BUFG_inst: BUFG
-- port map (
-- O => fclk2, -- 1-bit output: Clock output
-- I => fclk2a -- 1-bit input: Clock input
-- );

fclk2 <= fclk1;
 
--DCLK1_BUFFER: IBUFGDS
--generic map (
--   DIFF_TERM => TRUE, -- Differential Termination 
--   IBUF_LOW_PWR => FALSE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
--   IOSTANDARD => "DEFAULT")
--port map (
--   O => dclk1,  -- Buffer output
--   I => DCO1P,  -- Diff_p buffer input (connect directly to top-level port)
--   IB => DCO1M -- Diff_n buffer input (connect directly to top-level port)
--);

--DCLK2_BUFFER: IBUFGDS
--generic map (
--   DIFF_TERM => TRUE, -- Differential Termination 
--   IBUF_LOW_PWR => FALSE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
--   IOSTANDARD => "DEFAULT")
--port map (
--   O => dclk2,  -- Buffer output
--   I => DCO2P,  -- Diff_p buffer input (connect directly to top-level port)
--   IB => DCO2M -- Diff_n buffer input (connect directly to top-level port)
--);


LVDS_FCLK1 <= fclk1;
LVDS_FCLK2 <= fclk2;
	  
dclk1_adj:dclk_delay
port map(
      RESET     => RESET,
	  DCLKP_IN  => DCO1P,
	  DCLKN_IN  => DCO1M,
	  REFCLK_IN => CLK200,
	  
	  CNT_OUT    => dclk1_idly_cnt_out,
      CTRL_RDY   => dclk1_idly_ctrl_rdy,
      IDLY_LD    => dclk1_idly_ld,
      CNT_IN     => dclk1_idly_cnt_in,
 	  
	  DCLK_OUT  => dclk1
);

dclk2_adj:dclk_delay
port map(
      RESET     => RESET,
      DCLKP_IN  => DCO2P,
      DCLKN_IN  => DCO2M,
      REFCLK_IN => CLK200,
            
      CNT_OUT    => dclk2_idly_cnt_out,
      CTRL_RDY   => dclk2_idly_ctrl_rdy,
      IDLY_LD    => dclk2_idly_ld,
      CNT_IN     => dclk2_idly_cnt_in,
             
      DCLK_OUT  => dclk2
);	
	  
ad9249_chA1:ads52j90_lvds_ch
Port map (
    RESET => RESET,
	FCLK  => fclk1,
    DCLK  => dclk1,
    DOUTP => DOUTP_A1,
    DOUTM => DOUTM_A1,
    DATA_OUT => adc_ch_a1_data,
	latch_p_s  => fclk_pos_bank1
);

ad9249_chB1:ads52j90_lvds_ch
Port map (
    RESET => RESET,
	FCLK  => fclk1,
    DCLK  => dclk1,
    DOUTP => DOUTP_B1,
    DOUTM => DOUTM_B1,
    DATA_OUT => adc_ch_b1_data,
	latch_p_s  => fclk_pos_bank1
);
ad9249_chC1:ads52j90_lvds_ch
Port map (
    RESET => RESET,
	FCLK  => fclk1,
    DCLK  => dclk1,
    DOUTP => DOUTP_C1,
    DOUTM => DOUTM_C1,
    DATA_OUT => adc_ch_c1_data,
	latch_p_s  => fclk_pos_bank1
);
ad9249_chD1:ads52j90_lvds_ch
Port map (
    RESET => RESET,
	FCLK  => fclk1,
    DCLK  => dclk1,
    DOUTP => DOUTP_D1,
    DOUTM => DOUTM_D1,
    DATA_OUT => adc_ch_d1_data,
	latch_p_s  => fclk_pos_bank1
);
ad9249_chE1:ads52j90_lvds_ch
Port map (
    RESET => RESET,
	FCLK  => fclk1,
    DCLK  => dclk1,
    DOUTP => DOUTP_E1,
    DOUTM => DOUTM_E1,
    DATA_OUT => adc_ch_e1_data,
	latch_p_s  => fclk_pos_bank1
);
ad9249_chF1:ads52j90_lvds_ch
Port map (
    RESET => RESET,
	FCLK  => fclk1,
    DCLK  => dclk1,
    DOUTP => DOUTP_F1,
    DOUTM => DOUTM_F1,
    DATA_OUT => adc_ch_f1_data,
	latch_p_s  => fclk_pos_bank1
);
ad9249_chG1:ads52j90_lvds_ch
Port map (
    RESET => RESET,
	FCLK  => fclk1,
    DCLK  => dclk1,
    DOUTP => DOUTP_G1,
    DOUTM => DOUTM_G1,
    DATA_OUT => adc_ch_g1_data,
	latch_p_s  => fclk_pos_bank1
);
ad9249_chH1:ads52j90_lvds_ch
Port map (
    RESET => RESET,
	FCLK  => fclk1,
    DCLK  => dclk1,
    DOUTP => DOUTP_H1,
    DOUTM => DOUTM_H1,
    DATA_OUT => adc_ch_h1_data,
	latch_p_s  => fclk_pos_bank1
);

ad9249_chA2:ads52j90_lvds_ch
Port map (
    RESET => RESET,
	FCLK  => fclk2,
    DCLK  => dclk2,
    DOUTP => DOUTP_A2,
    DOUTM => DOUTM_A2,
    DATA_OUT => adc_ch_a2_data,
	latch_p_s  => fclk_pos_bank2
);

ad9249_chB2:ads52j90_lvds_ch
Port map (
    RESET => RESET,
	FCLK  => fclk2,
    DCLK  => dclk2,
    DOUTP => DOUTP_B2,
    DOUTM => DOUTM_B2,
    DATA_OUT => adc_ch_b2_data,
	latch_p_s  => fclk_pos_bank2
);
ad9249_chC2:ads52j90_lvds_ch
Port map (
    RESET => RESET,
	FCLK  => fclk2,
    DCLK  => dclk2,
    DOUTP => DOUTP_C2,
    DOUTM => DOUTM_C2,
    DATA_OUT => adc_ch_c2_data,
	latch_p_s  => fclk_pos_bank2
);
ad9249_chD2:ads52j90_lvds_ch
Port map (
    RESET => RESET,
	FCLK  => fclk2,
    DCLK  => dclk2,
    DOUTP => DOUTP_D2,
    DOUTM => DOUTM_D2,
    DATA_OUT => adc_ch_d2_data,
	latch_p_s  => fclk_pos_bank2
);
ad9249_chE2:ads52j90_lvds_ch
Port map (
    RESET => RESET,
	FCLK  => fclk2,
    DCLK  => dclk2,
    DOUTP => DOUTP_E2,
    DOUTM => DOUTM_E2,
    DATA_OUT => adc_ch_e2_data,
	latch_p_s  => fclk_pos_bank2
);
ad9249_chF2:ads52j90_lvds_ch
Port map (
    RESET => RESET,
	FCLK  => fclk2,
    DCLK  => dclk2,
    DOUTP => DOUTP_F2,
    DOUTM => DOUTM_F2,
    DATA_OUT => adc_ch_f2_data,
	latch_p_s  => fclk_pos_bank2
);
ad9249_chG2:ads52j90_lvds_ch
Port map (
    RESET => RESET,
	FCLK  => fclk2,
    DCLK  => dclk2,
    DOUTP => DOUTP_G2,
    DOUTM => DOUTM_G2,
    DATA_OUT => adc_ch_g2_data,
	latch_p_s  => fclk_pos_bank2
);
ad9249_chH2:ads52j90_lvds_ch
Port map (
    RESET => RESET,
	FCLK  => fclk2,
    DCLK  => dclk2,
    DOUTP => DOUTP_H2,
    DOUTM => DOUTM_H2,
    DATA_OUT => adc_ch_h2_data,
	latch_p_s  => fclk_pos_bank2
);



fclk1_counter:frequency_counter 
generic map
(
     SYS_CLOCK_PERIOD => 10 
)
port map 
( 
     CLOCK_PROBE => fclk1,
     SYSCLK      => SYSCLK,
     RESET       => tied_to_ground,
     FREQUENCY   => fclk1_freq
); 

dclk1_counter:frequency_counter 
       generic map
       (
         SYS_CLOCK_PERIOD => 10 
       )
       port map 
       ( 
         CLOCK_PROBE => dclk1,
         SYSCLK      => SYSCLK,
         RESET       => tied_to_ground,
         FREQUENCY   => dclk1_freq
        ); 

fclk2_counter:frequency_counter 
       generic map
       (
         SYS_CLOCK_PERIOD => 10 
       )
       port map 
       ( 
         CLOCK_PROBE => fclk1,
         SYSCLK      => SYSCLK,
         RESET       => tied_to_ground,
         FREQUENCY   => fclk2_freq
        ); 

dclk2_counter:frequency_counter 
       generic map
       (
         SYS_CLOCK_PERIOD => 10 
       )
       port map 
       ( 
         CLOCK_PROBE => dclk2,
         SYSCLK      => SYSCLK,
         RESET       => tied_to_ground,
         FREQUENCY   => dclk2_freq
        ); 
       
CH_A1_DATA_OUT(13 downto 0)  <= adc_ch_a1_data;
CH_B1_DATA_OUT(13 downto 0)  <= adc_ch_b1_data;
CH_C1_DATA_OUT(13 downto 0)  <= adc_ch_c1_data;
CH_D1_DATA_OUT(13 downto 0)  <= adc_ch_d1_data;
CH_E1_DATA_OUT(13 downto 0)  <= adc_ch_e1_data;
CH_F1_DATA_OUT(13 downto 0)  <= adc_ch_f1_data;
CH_G1_DATA_OUT(13 downto 0)  <= adc_ch_g1_data;
CH_H1_DATA_OUT(13 downto 0)  <= adc_ch_h1_data;
CH_A2_DATA_OUT(13 downto 0)  <= adc_ch_a2_data;
CH_B2_DATA_OUT(13 downto 0)  <= adc_ch_b2_data;
CH_C2_DATA_OUT(13 downto 0)  <= adc_ch_c2_data;
CH_D2_DATA_OUT(13 downto 0)  <= adc_ch_d2_data;
CH_E2_DATA_OUT(13 downto 0)  <= adc_ch_e2_data;
CH_F2_DATA_OUT(13 downto 0)  <= adc_ch_f2_data;
CH_G2_DATA_OUT(13 downto 0)  <= adc_ch_g2_data;
CH_H2_DATA_OUT(13 downto 0)  <= adc_ch_h2_data;

CH_A1_DATA_OUT(15 downto 14) <= (others => '0');
CH_B1_DATA_OUT(15 downto 14) <= (others => '0');
CH_C1_DATA_OUT(15 downto 14) <= (others => '0');
CH_D1_DATA_OUT(15 downto 14) <= (others => '0');
CH_E1_DATA_OUT(15 downto 14) <= (others => '0');
CH_F1_DATA_OUT(15 downto 14) <= (others => '0');
CH_G1_DATA_OUT(15 downto 14) <= (others => '0');
CH_H1_DATA_OUT(15 downto 14) <= (others => '0');
CH_A2_DATA_OUT(15 downto 14) <= (others => '0');
CH_B2_DATA_OUT(15 downto 14) <= (others => '0');
CH_C2_DATA_OUT(15 downto 14) <= (others => '0');
CH_D2_DATA_OUT(15 downto 14) <= (others => '0');
CH_E2_DATA_OUT(15 downto 14) <= (others => '0');
CH_F2_DATA_OUT(15 downto 14) <= (others => '0');
CH_G2_DATA_OUT(15 downto 14) <= (others => '0');
CH_H2_DATA_OUT(15 downto 14) <= (others => '0');


end Behavioral;
