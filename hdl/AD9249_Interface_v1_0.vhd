library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library xil_defaultlib;
use xil_defaultlib.all;

entity AD9249_Interface_v1_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line

		-- Width of S_AXIS address bus. The slave accepts the read and write addresses of width C_M_AXIS_TDATA_WIDTH.
		C_M_AXIS_TDATA_WIDTH	: integer	:= 32;
		-- Start count is the number of clock cycles the master will wait before initiating/issuing any transaction.
		C_M_START_COUNT	: integer	:= 32
	);
	port (
    clk100m                                    : IN STD_LOGIC;
    clk200m                                    : IN STD_LOGIC;
	
	
	    -- ADC Interface                 
    ADC_FCO1P                                    : IN STD_LOGIC;
    ADC_FCO1M                                    : IN STD_LOGIC;
    ADC_FCO2P                                    : IN STD_LOGIC;
    ADC_FCO2M                                    : IN STD_LOGIC;
    
    ADC_DCO1P                                    : IN STD_LOGIC;
    ADC_DCO1M                                    : IN STD_LOGIC;
    ADC_DCO2P                                    : IN STD_LOGIC;
    ADC_DCO2M                                    : IN STD_LOGIC;
    
    ADC_DOUTP_A1                                 : IN STD_LOGIC;
    ADC_DOUTM_A1                                 : IN STD_LOGIC;
    ADC_DOUTP_A2                                 : IN STD_LOGIC;
    ADC_DOUTM_A2                                 : IN STD_LOGIC;
    ADC_DOUTP_B1                                 : IN STD_LOGIC;
    ADC_DOUTM_B1                                 : IN STD_LOGIC;
    ADC_DOUTP_B2                                 : IN STD_LOGIC;
    ADC_DOUTM_B2                                 : IN STD_LOGIC;
    ADC_DOUTP_C1                                 : IN STD_LOGIC;
    ADC_DOUTM_C1                                 : IN STD_LOGIC;
    ADC_DOUTP_C2                                 : IN STD_LOGIC;
    ADC_DOUTM_C2                                 : IN STD_LOGIC;
    ADC_DOUTP_D1                                 : IN STD_LOGIC;
    ADC_DOUTM_D1                                 : IN STD_LOGIC;
    ADC_DOUTP_D2                                 : IN STD_LOGIC;
    ADC_DOUTM_D2                                 : IN STD_LOGIC;
    ADC_DOUTP_E1                                 : IN STD_LOGIC;
    ADC_DOUTM_E1                                 : IN STD_LOGIC;
    ADC_DOUTP_E2                                 : IN STD_LOGIC;
    ADC_DOUTM_E2                                 : IN STD_LOGIC;
    ADC_DOUTP_F1                                 : IN STD_LOGIC;
    ADC_DOUTM_F1                                 : IN STD_LOGIC;
    ADC_DOUTP_F2                                 : IN STD_LOGIC;
    ADC_DOUTM_F2                                 : IN STD_LOGIC;
    ADC_DOUTP_G1                                 : IN STD_LOGIC;
    ADC_DOUTM_G1                                 : IN STD_LOGIC;
    ADC_DOUTP_G2                                 : IN STD_LOGIC;
    ADC_DOUTM_G2                                 : IN STD_LOGIC;
    ADC_DOUTP_H1                                 : IN STD_LOGIC;
    ADC_DOUTM_H1                                 : IN STD_LOGIC;
    ADC_DOUTP_H2                                 : IN STD_LOGIC;
    ADC_DOUTM_H2                                 : IN STD_LOGIC;	
	
    axi_clk 		: in std_logic;
    m1_axi_reset         : in std_logic;
    m1_addr_rst        : in std_logic;
         
     
    m1_axi_awready     : in std_logic;
    m1_axi_awaddr         : out std_logic_vector(31 downto 0);--32bit-->28bit: top.vhd assign high 4 bit
    m1_axi_awvalid     : out std_logic;     
    m1_axi_awlen         : out std_logic_vector(7 downto 0);
    m1_axi_awsize         : out std_logic_vector(2 downto 0);
    m1_axi_awburst     : out std_logic_vector(1 downto 0);
    m1_axi_awcache     : out std_logic_vector(3 downto 0);
    m1_axi_awprot         : out std_logic_vector(2 downto 0);
 
    m1_axi_wready         : in std_logic;
    m1_axi_wdata         : out std_logic_vector(63 downto 0);
    m1_axi_wvalid         : out std_logic;
    m1_axi_wstrb         : out std_logic_vector(7 downto 0);
    m1_axi_wlast         : out std_logic;

    m1_axi_bvalid         : in std_logic; 
    m1_axi_bresp         : in std_logic_vector(1 downto 0);
    m1_axi_bready         : out std_logic;
    
    m2_axi_reset         : in std_logic;
    m2_addr_rst        : in std_logic;
            
     
    m2_axi_awready     : in std_logic;
    m2_axi_awaddr         : out std_logic_vector(31 downto 0);--32bit-->28bit: top.vhd assign high 4 bit
    m2_axi_awvalid     : out std_logic;     
    m2_axi_awlen         : out std_logic_vector(7 downto 0);
    m2_axi_awsize         : out std_logic_vector(2 downto 0);
    m2_axi_awburst     : out std_logic_vector(1 downto 0);
    m2_axi_awcache     : out std_logic_vector(3 downto 0);
    m2_axi_awprot         : out std_logic_vector(2 downto 0);
 
    m2_axi_wready         : in std_logic;
    m2_axi_wdata         : out std_logic_vector(63 downto 0);
    m2_axi_wvalid         : out std_logic;
    m2_axi_wstrb         : out std_logic_vector(7 downto 0);
    m2_axi_wlast         : out std_logic;

    m2_axi_bvalid         : in std_logic; 
    m2_axi_bresp         : in std_logic_vector(1 downto 0);
    m2_axi_bready         : out std_logic
	);
end AD9249_Interface_v1_0;

architecture implementation of AD9249_Interface_v1_0_M00_AXIS is
signal m1_data_gen_burst_active :std_logic;
signal m2_data_gen_burst_active :std_logic;

signal m1_fifo_reset      :  std_logic;       
signal m1_fifo_wrclk      :  std_logic;
signal m1_fifo_data       :  std_logic_vector(127 downto 0);
signal m1_fifo_wren       :  std_logic;
signal m1_fifo_wrcnt      :  std_logic_vector(9 downto 0);  
signal m1_burst_addr      :  std_logic_vector(31 downto 0);  
  
signal m2_fifo_reset      :  std_logic;       
signal m2_fifo_wrclk      :  std_logic;
signal m2_fifo_data       :  std_logic_vector(127 downto 0);
signal m2_fifo_wren       :  std_logic;
signal m2_fifo_wrcnt      :  std_logic_vector(9 downto 0);  
signal m2_burst_addr      :  std_logic_vector(31 downto 0); 

--ADC LVDS data signals
signal adc_lvds_ch_a1_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_a2_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_b1_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_b2_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_c1_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_c2_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_d1_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_d2_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_e1_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_e2_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_f1_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_f2_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_g1_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_g2_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_h1_data     :std_logic_vector(15 downto 0);
signal adc_lvds_ch_h2_data     :std_logic_vector(15 downto 0);
--attribute MARK_DEBUG of adc_lvds_ch_a1_data, adc_lvds_ch_a2_data, adc_lvds_ch_b1_data, adc_lvds_ch_b2_data, adc_lvds_ch_c1_data, adc_lvds_ch_c2_data, adc_lvds_ch_d1_data, adc_lvds_ch_d2_data : signal is "TRUE";
--attribute MARK_DEBUG of adc_lvds_ch_e1_data, adc_lvds_ch_e2_data, adc_lvds_ch_f1_data, adc_lvds_ch_f2_data, adc_lvds_ch_g1_data, adc_lvds_ch_g2_data, adc_lvds_ch_h1_data, adc_lvds_ch_h2_data : signal is "TRUE"; 
signal adc_lvds_ch_a1_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_a2_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_b1_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_b2_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_c1_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_c2_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_d1_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_d2_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_e1_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_e2_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_f1_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_f2_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_g1_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_g2_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_h1_data_dly :std_logic_vector(15 downto 0);
signal adc_lvds_ch_h2_data_dly :std_logic_vector(15 downto 0);

signal adc_fclk_bank1          :std_logic;
signal adc_fclk_bank2          :std_logic;
signal adc_clk_dis_t           :std_logic;--1 disable the adc sampling clock output
signal adc_fclk_pos            :std_logic_vector(3 downto 0);
signal adc_dclk_freq_t         :std_logic_vector(19 downto 0);
signal adc_fclk_freq_t         :std_logic_vector(19 downto 0);
signal adc_idly_cnt_out_t      :std_logic_vector(4 downto 0);
signal adc_idly_ctrl_rdy_t     :std_logic;
signal adc_idly_ld_t           :std_logic;
signal adc_idly_cnt_in_t       :std_logic_vector(4 downto 0);
signal adc_bank_sel_t          :std_logic; 


signal m1_data_gen_trig       :std_logic;
signal m1_data_gen_adcburstlen:std_logic_vector(31 downto 0);
signal m2_data_gen_trig       :std_logic;
signal m2_data_gen_adcburstlen:std_logic_vector(31 downto 0);
signal data_gen_testdata_en :std_logic;
signal data_gen_adcburstenb :std_logic_vector(3 downto 0);
signal data_gen_softreset   :std_logic_vector(3 downto 0);

signal tied_to_ground : std_logic;

begin

tied_to_ground <= '0';


-- ADC Data Trans.  
-- Address: 1000_0000 to 1FFF_FFFF     
axi_hp0_burst: entity axi_mburst
    Generic map
    (
    start_addr => X"000_0000",
    end_addr => X"7FF_FFFF"
    )
    port map(
      axi_clk       => axi_clk,
      axi_reset     => m1_axi_reset,
      
      addr_rst      => m1_addr_rst,
      fifo_reset    => m1_fifo_reset, 
      
      fifo_wrclk    => m1_fifo_wrclk,
      fifo_data     => m1_fifo_data,
      fifo_wren     => m1_fifo_wren,
      fifo_wrcnt    => m1_fifo_wrcnt, --iobus  
      burst_addr    => m1_burst_addr,   
       
      axi_awready   => m1_axi_awready,
      axi_awaddr    => m1_axi_awaddr,
      axi_awvalid   => m1_axi_awvalid,
      axi_awlen     => m1_axi_awlen,
      axi_awsize    => m1_axi_awsize,
      axi_awburst   => m1_axi_awburst,
      axi_awcache   => m1_axi_awcache,
      axi_awprot    => m1_axi_awprot,
   
      axi_wready    => m1_axi_wready,
      axi_wdata     => m1_axi_wdata,
      axi_wvalid    => m1_axi_wvalid,
      axi_wstrb     => m1_axi_wstrb,
      axi_wlast     => m1_axi_wlast,
  
      axi_bvalid    => m1_axi_bvalid,
      axi_bresp     => m1_axi_bresp,
      axi_bready    => m1_axi_bready
);


axi_hp1_burst: entity axi_mburst
    Generic map
    (
    start_addr => X"000_0000",
    end_addr => X"7FF_FFFF"
    )
    port map(
      axi_clk       => axi_clk,
      axi_reset     => m2_axi_reset,
      
      addr_rst      => m2_addr_rst,
      fifo_reset    => m2_fifo_reset, 
      
      fifo_wrclk    => m2_fifo_wrclk,
      fifo_data     => m2_fifo_data,
      fifo_wren     => m2_fifo_wren,
      fifo_wrcnt    => m2_fifo_wrcnt, --iobus  
      burst_addr    => m2_burst_addr,   
       
      axi_awready   => m2_axi_awready,
      axi_awaddr    => m2_axi_awaddr,
      axi_awvalid   => m2_axi_awvalid,
      axi_awlen     => m2_axi_awlen,
      axi_awsize    => m2_axi_awsize,
      axi_awburst   => m2_axi_awburst,
      axi_awcache   => m2_axi_awcache,
      axi_awprot    => m2_axi_awprot,
   
      axi_wready    => m2_axi_wready,
      axi_wdata     => m2_axi_wdata,
      axi_wvalid    => m2_axi_wvalid,
      axi_wstrb     => m2_axi_wstrb,
      axi_wlast     => m2_axi_wlast,
  
      axi_bvalid    => m2_axi_bvalid,
      axi_bresp     => m2_axi_bresp,
      axi_bready    => m2_axi_bready
);

data_gen_hp0:entity work.axi_data_gen   
  port map(
     adc_clk             => adc_fclk_bank1, 
     reset               => data_gen_softreset(0),   --iobus
                         
     trig                => m1_data_gen_trig,    --iobus
     adcburst_enb        => data_gen_adcburstenb(0), --iobus
     adcburst_len        => m1_data_gen_adcburstlen, --iobus
     testdata_en         => data_gen_testdata_en, --iobus     
 
     adc_ch0             => adc_lvds_ch_a1_data, 
     adc_ch1             => adc_lvds_ch_b1_data, 
     adc_ch2             => adc_lvds_ch_c1_data, 
     adc_ch3             => adc_lvds_ch_d1_data, 
     adc_ch4             => adc_lvds_ch_e1_data, 
     adc_ch5             => adc_lvds_ch_f1_data, 
     adc_ch6             => adc_lvds_ch_g1_data,  
     adc_ch7             => adc_lvds_ch_h1_data,    
     
     adcdata             => m1_fifo_data, 
     adcdata_wren        => m1_fifo_wren, 
     adcburst_active     => m1_data_gen_burst_active       --ila
  );  

data_gen_hp1:entity axi_data_gen   
  port map(
     adc_clk             => adc_fclk_bank2, 
     reset               => data_gen_softreset(1),   --iobus
                         
     trig                => m2_data_gen_trig,    --iobus
     adcburst_enb        => data_gen_adcburstenb(1), --iobus
     adcburst_len        => m2_data_gen_adcburstlen, --iobus
     testdata_en         => data_gen_testdata_en, --iobus     

     adc_ch0             => adc_lvds_ch_a2_data, 
     adc_ch1             => adc_lvds_ch_b2_data, 
     adc_ch2             => adc_lvds_ch_c2_data, 
     adc_ch3             => adc_lvds_ch_d2_data, 
     adc_ch4             => adc_lvds_ch_e2_data, 
     adc_ch5             => adc_lvds_ch_f2_data, 
     adc_ch6             => adc_lvds_ch_g2_data,  
     adc_ch7             => adc_lvds_ch_h2_data,    
     
     adcdata             => m2_fifo_data, 
     adcdata_wren        => m2_fifo_wren, 
     adcburst_active     => m2_data_gen_burst_active       --ila
  );  

ad9249_lvds_input: entity work.ad9249_lvds 
Port map ( 
    RESET   => tied_to_ground,
    SYSCLK  =>   clk100m,
    CLK200  =>   clk200m,
    
    FCO1P   => ADC_FCO1P,
    FCO1M   => ADC_FCO1M,
    FCO2P   => ADC_FCO2P,
    FCO2M   => ADC_FCO2M,
        
    DCO1P   => ADC_DCO1P,
    DCO1M   => ADC_DCO1M,
    DCO2P   => ADC_DCO2P,
    DCO2M   => ADC_DCO2M,
        
    DOUTP_A1  =>  ADC_DOUTP_A1,
    DOUTM_A1  =>  ADC_DOUTM_A1,
    DOUTP_A2  =>  ADC_DOUTP_A2,
    DOUTM_A2  =>  ADC_DOUTM_A2,
    DOUTP_B1  =>  ADC_DOUTP_B1,
    DOUTM_B1  =>  ADC_DOUTM_B1,
    DOUTP_B2  =>  ADC_DOUTP_B2,
    DOUTM_B2  =>  ADC_DOUTM_B2,
    DOUTP_C1  =>  ADC_DOUTP_C1,
    DOUTM_C1  =>  ADC_DOUTM_C1,
    DOUTP_C2  =>  ADC_DOUTP_C2,
    DOUTM_C2  =>  ADC_DOUTM_C2,
    DOUTP_D1  =>  ADC_DOUTP_D1,
    DOUTM_D1  =>  ADC_DOUTM_D1,
    DOUTP_D2  =>  ADC_DOUTP_D2,
    DOUTM_D2  =>  ADC_DOUTM_D2,
    DOUTP_E1  =>  ADC_DOUTP_E1,
    DOUTM_E1  =>  ADC_DOUTM_E1,
    DOUTP_E2  =>  ADC_DOUTP_E2,
    DOUTM_E2  =>  ADC_DOUTM_E2,
    DOUTP_F1  =>  ADC_DOUTP_F1,
    DOUTM_F1  =>  ADC_DOUTM_F1,
    DOUTP_F2  =>  ADC_DOUTP_F2,
    DOUTM_F2  =>  ADC_DOUTM_F2,
    DOUTP_G1  =>  ADC_DOUTP_G1,
    DOUTM_G1  =>  ADC_DOUTM_G1,
    DOUTP_G2  =>  ADC_DOUTP_G2,
    DOUTM_G2  =>  ADC_DOUTM_G2,
    DOUTP_H1  =>  ADC_DOUTP_H1,
    DOUTM_H1  =>  ADC_DOUTM_H1,
    DOUTP_H2  =>  ADC_DOUTP_H2,
    DOUTM_H2  =>  ADC_DOUTM_H2,
    
    CH_A1_DATA_OUT => adc_lvds_ch_a1_data,
    CH_A2_DATA_OUT => adc_lvds_ch_a2_data,
    CH_B1_DATA_OUT => adc_lvds_ch_b1_data,
    CH_B2_DATA_OUT => adc_lvds_ch_b2_data,
    CH_C1_DATA_OUT => adc_lvds_ch_c1_data,
    CH_C2_DATA_OUT => adc_lvds_ch_c2_data,
    CH_D1_DATA_OUT => adc_lvds_ch_d1_data,
    CH_D2_DATA_OUT => adc_lvds_ch_d2_data,
    CH_E1_DATA_OUT => adc_lvds_ch_e1_data,
    CH_E2_DATA_OUT => adc_lvds_ch_e2_data,
    CH_F1_DATA_OUT => adc_lvds_ch_f1_data,
    CH_F2_DATA_OUT => adc_lvds_ch_f2_data,
    CH_G1_DATA_OUT => adc_lvds_ch_g1_data,
    CH_G2_DATA_OUT => adc_lvds_ch_g2_data,
    CH_H1_DATA_OUT => adc_lvds_ch_h1_data,
    CH_H2_DATA_OUT => adc_lvds_ch_h2_data,
 
    LVDS_FCLK1     => adc_fclk_bank1,
    LVDS_FCLK2     => adc_fclk_bank2,
    
    BANK_SEL       => adc_bank_sel_t,
    FCLK_POS       => adc_fclk_pos,
    DCLKFREQ       => adc_dclk_freq_t,
    FCLKFREQ       => adc_fclk_freq_t,
    
    IDLY_CNT_OUT    => adc_idly_cnt_out_t,
    IDLY_CTRL_RDY   => adc_idly_ctrl_rdy_t,
    IDLY_LD         => adc_idly_ld_t,
    IDLY_CNT_IN     => adc_idly_cnt_in_t          
); 

end implementation;
